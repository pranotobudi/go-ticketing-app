package redis

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"gitlab.com/pranotobudi/go-ticketing-app/database"
)

var ctx = context.Background()
var rdb = redis.NewClient(&redis.Options{
	Addr:     "localhost:6379",
	Password: "", // no password set
	DB:       0,  // use default DB
})

func GetRedisInstance() *redis.Client {
	return rdb
}
func ExampleClient() {
	fmt.Println("=================EXAMPLE CLIENT =============")

	err := rdb.Set(ctx, "key", "value", 0).Err()
	if err != nil {
		panic(err)
	}

	val, err := rdb.Get(ctx, "key").Result()
	if err != nil {
		panic(err)
	}
	fmt.Println("key", val)

	val2, err := rdb.Get(ctx, "key2").Result()
	if err == redis.Nil {
		fmt.Println("key2 does not exist")
	} else if err != nil {
		panic(err)
	} else {
		fmt.Println("key2", val2)
	}
	// Output: key value
	// key2 does not exist
}

// func ValueExists(key string) (string, bool) {
// 	val2, err := rdb.Get(ctx, key).Result()
// 	if err == redis.Nil {
// 		fmt.Println("key2 does not exist")
// 		return "", false
// 	} else if err != nil {
// 		panic(err)
// 		return "", false
// 	} else {
// 		fmt.Println("key2", val2)
// 	}
// 	return true
// }
func CacheMiddlewareGeneric(c *gin.Context) {
	rdb.Exists(ctx, c.Request.URL.Path)
	redisVal, err := rdb.Get(ctx, c.Request.URL.Path).Result()
	if err != nil {
		fmt.Println("====CACHE MIDDLEWARE: NEXT========")
		c.Next()
		return
	}

	fmt.Println("====CACHE MIDDLEWARE: val: ========")
	var dest interface{}
	json.Unmarshal([]byte(redisVal), &dest)
	c.JSON(http.StatusOK, dest)
	c.Abort()

}

func CacheMiddlewareByTypeReturned(object interface{}) gin.HandlerFunc {
	return CacheMiddlewareObject(object)
}
func CacheMiddlewareObject(object interface{}) gin.HandlerFunc {
	return func(c *gin.Context) {
		rdb.Exists(ctx, c.Request.URL.Path)
		redisVal, err := rdb.Get(ctx, c.Request.URL.Path).Result()
		if err != nil {
			fmt.Println("====CACHE MIDDLEWARE: NEXT========")
			c.Next()
			return
		}

		fmt.Println("====CACHE MIDDLEWARE: val: ========")
		switch object.(type) {
		case []database.User:
			var dest []database.User
			json.Unmarshal([]byte(redisVal), &dest)
			c.JSON(http.StatusOK, dest)
		case database.User:
			var dest database.User
			json.Unmarshal([]byte(redisVal), &dest)
			c.JSON(http.StatusOK, dest)
		case []database.Event:
			var dest []database.Event
			json.Unmarshal([]byte(redisVal), &dest)
			c.JSON(http.StatusOK, dest)
		case database.Event:
			var dest database.Event
			json.Unmarshal([]byte(redisVal), &dest)
			c.JSON(http.StatusOK, dest)
		}
		// var dest interface{}

		// json.Unmarshal(val, dest)
		// var dest interface{}
		c.Abort()

	}
}

func CacheUserMiddleware(c *gin.Context) {
	rdb.Exists(ctx, c.Request.URL.Path)
	redisVal, err := rdb.Get(ctx, c.Request.URL.Path).Result()
	if err != nil {
		fmt.Println("====CACHE MIDDLEWARE: NEXT========")
		c.Next()
		return
	}
	// json.Unmarshal(val, dest)
	fmt.Println("====CACHE MIDDLEWARE: val: ========")
	var dest database.User
	// var dest interface{}
	json.Unmarshal([]byte(redisVal), &dest)
	c.JSON(http.StatusOK, dest)
	c.Abort()
}

func CacheUsersMiddleware(c *gin.Context) {
	rdb.Exists(ctx, c.Request.URL.Path)
	redisVal, err := rdb.Get(ctx, c.Request.URL.Path).Result()
	if err != nil {
		fmt.Println("====CACHE MIDDLEWARE: NEXT========")
		c.Next()
		return
	}
	// json.Unmarshal(val, dest)
	fmt.Println("====CACHE MIDDLEWARE: val: ========")
	var dest []database.User
	// var dest interface{}
	json.Unmarshal([]byte(redisVal), &dest)
	c.JSON(http.StatusOK, dest)
	c.Abort()
}

func CacheEventsMiddleware(c *gin.Context) {
	rdb.Exists(ctx, c.Request.URL.Path)
	redisVal, err := rdb.Get(ctx, c.Request.URL.Path).Result()
	if err != nil {
		fmt.Println("====CACHE MIDDLEWARE: NEXT========")
		c.Next()
		return
	}
	// json.Unmarshal(val, dest)
	fmt.Println("====CACHE MIDDLEWARE: val: ========")
	var dest []database.Event
	// var dest interface{}
	json.Unmarshal([]byte(redisVal), &dest)
	c.JSON(http.StatusOK, dest)
	c.Abort()
}

func CacheTransactionsMiddleware(c *gin.Context) {
	rdb.Exists(ctx, c.Request.URL.Path)
	redisVal, err := rdb.Get(ctx, c.Request.URL.Path).Result()
	if err != nil {
		fmt.Println("====CACHE MIDDLEWARE: NEXT========")
		c.Next()
		return
	}
	// json.Unmarshal(val, dest)
	fmt.Println("====CACHE MIDDLEWARE: val: ========")
	var dest []database.Transaction
	// var dest interface{}
	json.Unmarshal([]byte(redisVal), &dest)
	c.JSON(http.StatusOK, dest)
	c.Abort()
}

func CacheReportMoneyMiddleware(c *gin.Context) {
	rdb.Exists(ctx, c.Request.URL.Path)
	redisVal, err := rdb.Get(ctx, c.Request.URL.Path).Result()
	if err != nil {
		fmt.Println("====CACHE MIDDLEWARE: NEXT========")
		c.Next()
		return
	}
	// json.Unmarshal(val, dest)
	fmt.Println("====CACHE MIDDLEWARE: val: ========")
	var dest []database.Report
	// var dest interface{}
	json.Unmarshal([]byte(redisVal), &dest)
	c.JSON(http.StatusOK, dest)
	c.Abort()
}
func CacheReportParticipantMiddleware(c *gin.Context) {
	rdb.Exists(ctx, c.Request.URL.Path)
	redisVal, err := rdb.Get(ctx, c.Request.URL.Path).Result()
	if err != nil {
		fmt.Println("====CACHE MIDDLEWARE: NEXT========")
		c.Next()
		return
	}
	// json.Unmarshal(val, dest)
	fmt.Println("====CACHE MIDDLEWARE: val: ========")
	var dest []database.Report
	// var dest interface{}
	json.Unmarshal([]byte(redisVal), &dest)
	c.JSON(http.StatusOK, dest)
	c.Abort()
}

func SetKeyValue(key string, value interface{}) error {
	err := rdb.Set(ctx, key, value, 0).Err()
	if err != nil {
		return err
	}
	return nil
}

func DelKey(key string) error {
	err := rdb.Del(ctx, key).Err()
	if err != nil {
		return err
	}
	return nil
}
