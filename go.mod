module gitlab.com/pranotobudi/go-ticketing-app

go 1.16

require (
	github.com/bxcodec/faker/v3 v3.6.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.1
	github.com/go-co-op/gocron v1.5.0 // indirect
	github.com/go-redis/redis/v8 v8.8.2
	github.com/google/uuid v1.2.0 // indirect
	github.com/hibiken/asynq v0.17.1
	github.com/joho/godotenv v1.3.0
	github.com/jordan-wright/email v4.0.1-0.20210109023952-943e75fe5223+incompatible // indirect
	github.com/lib/pq v1.3.0
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cobra v0.0.5 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.0 // indirect
	github.com/thanhpk/randstr v1.0.4
	golang.org/x/sys v0.0.0-20210426230700-d19ff857e887 // indirect
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba // indirect
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.8
)
