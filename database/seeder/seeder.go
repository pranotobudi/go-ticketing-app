package seeder

import (
	"math/rand"
	"time"

	"github.com/bxcodec/faker/v3"
	"gorm.io/gorm"
)

func DBSeed(totalUser int, totalEvent int, totalTransaction int, db *gorm.DB) error {
	defaultUserDataSeedInit(db)
	defaultEventDataSeedInit(db)
	defaultTransactionDataSeedInit(db)
	userDataSeedInit(totalUser, db)
	eventDataSeedInit(totalEvent, totalUser, db)
	transactionDataSeedInit(totalTransaction, totalUser, totalEvent, db)
	return nil
}
func defaultUserDataSeedInit(db *gorm.DB) {
	statement := "INSERT INTO users (username, fullname, email, password, role, deleted_at, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"

	db.Exec(statement, "username1", "fullusername1", "email1@gmail.com", "password1", "admin", faker.Timestamp(), faker.Timestamp(), faker.Timestamp())
	db.Exec(statement, "username2", "fullusername2", "email2@gmail.com", "password2", "creator", faker.Timestamp(), faker.Timestamp(), faker.Timestamp())
	db.Exec(statement, "username3", "fullusername3", "oceankingdigital@gmail.com", "password3", "participant", faker.Timestamp(), faker.Timestamp(), faker.Timestamp())
	db.Exec(statement, "username4", "fullusername4", "email4@gmail.com", "password4", "participant", faker.Timestamp(), faker.Timestamp(), faker.Timestamp())
	db.Exec(statement, "username5", "fullusername5", "email5@gmail.com", "password5", "participant", faker.Timestamp(), faker.Timestamp(), faker.Timestamp())
}

func defaultEventDataSeedInit(db *gorm.DB) {
	statement := "INSERT INTO events (creator_id, title_event, link_webinar, description, banner, price, quantity, status, event_start_date, event_end_date, campaign_start_date, campaign_end_date, deleted_at, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

	db.Exec(statement, 2, "Event 1", "/event1/webinar", "Event 1 Description", "/banner/url", 25000, 25, "release", time.Now().Add(time.Hour*24+time.Second*10), time.Now().Add(time.Hour*25), time.Now().Add(time.Hour*6), time.Now().Add(time.Hour*12), faker.Timestamp(), faker.Timestamp(), faker.Timestamp())
	db.Exec(statement, 2, "Event 2", "/event2/webinar", "Event 2 Description", "/banner/url", 25000, 25, "release", time.Now().Add(time.Hour*24+time.Second*15), time.Now().Add(time.Hour*25), time.Now().Add(time.Hour*6), time.Now().Add(time.Hour*12), faker.Timestamp(), faker.Timestamp(), faker.Timestamp())
	db.Exec(statement, 2, "Event 3", "/event3/webinar", "Event 3 Description", "/banner/url", 25000, 25, "release", time.Now().Add(time.Hour*24+time.Second*20), time.Now().Add(time.Hour*25), time.Now().Add(time.Hour*6), time.Now().Add(time.Hour*12), faker.Timestamp(), faker.Timestamp(), faker.Timestamp())
}

func defaultTransactionDataSeedInit(db *gorm.DB) {
	statement := "INSERT INTO transactions (participant_id, creator_id, event_id, amount, status_payment, deleted_at, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"

	db.Exec(statement, 3, 2, 1, 100000, "failed", faker.Timestamp(), faker.Timestamp(), faker.Timestamp(), faker.Timestamp())
	db.Exec(statement, 4, 2, 1, 100000, "passed", faker.Timestamp(), faker.Timestamp(), faker.Timestamp(), faker.Timestamp())
	db.Exec(statement, 5, 2, 1, 100000, "passed", faker.Timestamp(), faker.Timestamp(), faker.Timestamp(), faker.Timestamp())
}

func userDataSeedInit(totalUser int, db *gorm.DB) {
	statement := "INSERT INTO users (username, fullname, email, password, role, deleted_at, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
	for i := 0; i < totalUser; i++ {
		randSource := rand.NewSource(time.Now().UnixNano())
		randNumForUser := rand.New(randSource).Intn(3) + 0
		role := []string{"admin", "creator", "participant"}
		db.Exec(statement, faker.Username(), faker.Name(), faker.Email(), faker.Password(), role[randNumForUser], faker.Timestamp(), faker.Timestamp(), faker.Timestamp())
	}
}

func eventDataSeedInit(totalEvent int, totalUser int, db *gorm.DB) {
	statement := "INSERT INTO events (creator_id, title_event, link_webinar, description, banner, price, quantity, status, event_start_date, event_end_date, campaign_start_date, campaign_end_date, deleted_at, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

	for i := 0; i < totalEvent; i++ {
		//randomSource CreatorID
		randSource1 := rand.NewSource(time.Now().UnixNano())
		randNumForCreator := rand.New(randSource1).Intn(totalUser) + 1
		//randomSource Status
		randSource2 := rand.NewSource(time.Now().UnixNano())
		randNumForStatus := rand.New(randSource2).Intn(2) + 0
		var statusList = []string{"draft", "release"}
		//randomSource Price
		randSource3 := rand.NewSource(time.Now().UnixNano())
		randNumForPrice := rand.New(randSource3).Intn(100000) + 1
		//randomSource Quantity
		randSource4 := rand.NewSource(time.Now().UnixNano())
		randNumForQuantity := rand.New(randSource4).Intn(100) + 1

		db.Exec(statement, randNumForCreator, faker.Sentence(), faker.URL(), faker.Paragraph(), faker.URL(), randNumForPrice, randNumForQuantity, statusList[randNumForStatus], faker.Timestamp(), faker.Timestamp(), faker.Timestamp(), faker.Timestamp(), faker.Timestamp(), faker.Timestamp(), faker.Timestamp())
	}
}

func transactionDataSeedInit(totalTransaction int, totalUser int, totalEvent int, db *gorm.DB) {
	statement := "INSERT INTO transactions (participant_id, creator_id, event_id, amount, status_payment, deleted_at, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"

	for i := 0; i < totalEvent; i++ {
		//randomSource ParticipantID
		randSource1 := rand.NewSource(time.Now().UnixNano())
		randNumForParticipant := rand.New(randSource1).Intn(totalUser) + 1

		//randomSource CreatorID
		randSource5 := rand.NewSource(time.Now().UnixNano())
		randNumForCreator := rand.New(randSource5).Intn(totalUser) + 1

		//randomSource EventID
		randSource2 := rand.NewSource(time.Now().UnixNano())
		randNumForEvent := rand.New(randSource2).Intn(totalEvent) + 1
		//randomSource Amount
		randSource3 := rand.NewSource(time.Now().UnixNano())
		randNumForAmount := rand.New(randSource3).Intn(100000) + 1
		//randomSource StatusPayment
		randSource4 := rand.NewSource(time.Now().UnixNano())
		randNumForStatus := rand.New(randSource4).Intn(2) + 0
		var statusList = []string{"passed", "failed"}
		db.Exec(statement, randNumForParticipant, randNumForCreator, randNumForEvent, randNumForAmount, statusList[randNumForStatus], faker.Timestamp(), faker.Timestamp(), faker.Timestamp(), faker.Timestamp())
	}
}
