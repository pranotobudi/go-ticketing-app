package database

import (
	"database/sql"
	"fmt"
	"sort"
	"time"

	"github.com/thanhpk/randstr"
	"gitlab.com/pranotobudi/go-ticketing-app/email"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type DBType struct {
	DB *sql.DB
}

const (
	admin       string = "admin"
	creator     string = "creator"
	participant string = "participant"
)

type User struct {
	ID            uint
	Username      string `gorm:"unique"`
	Fullname      string
	Email         string `gorm:"unique"`
	Password      string
	Role          string
	Deleted_at    time.Time
	Created_at    time.Time
	Updated_at    time.Time
	Events        []Event        `gorm:"foreignKey:CreatorID; constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	Transactions  []Transaction  `gorm:"foreignKey:ParticipantID; constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	Registrations []Registration `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}

const (
	draft   string = "draft"
	release string = "release"
)

type Event struct {
	ID                  uint
	CreatorID           uint
	Title_event         string
	Link_webinar        string
	Description         string
	Banner              string
	Price               int64
	Quantity            int
	Status              string
	Event_start_date    time.Time
	Event_end_date      time.Time
	Campaign_start_date time.Time
	Campaign_end_date   time.Time
	Deleted_at          time.Time
	Created_at          time.Time
	Updated_at          time.Time
	Transactions        []Transaction `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}

const (
	passed string = "passed"
	failed string = "failed"
)

type Transaction struct {
	ID             int
	ParticipantID  int
	CreatorID      int
	EventID        uint
	Amount         int64
	Status_payment string
	Deleted_at     time.Time
	Created_at     time.Time
	Updated_at     time.Time
}

type Report struct {
	TotalParticipant int
	TotalMoney       int64
	DetailWebinar    Event
	ParticipantLists []User
}

type Registration struct {
	gorm.Model
	RegistrationToken string
	TimeCreated       time.Time
	UserID            uint
}

type DBTraining struct {
	gorm.Model
	Code  string `gorm:"index"`
	Price uint   `json:"price-standard"`
}

var db *gorm.DB

func OpenDB(dataSourceName string) (db *gorm.DB, err error) {
	fmt.Println("datasourceName: ", dataSourceName)
	return gorm.Open(postgres.Open(dataSourceName), &gorm.Config{})
}
func GetDB() *gorm.DB {
	return db
}
func StartDB(dataSourceName string) error {
	fmt.Println("datasourceName: ", dataSourceName)
	var err error
	db, err = gorm.Open(postgres.Open(dataSourceName), &gorm.Config{})
	return err
}

func InitDBTable() {
	db.AutoMigrate(&User{}, &Event{}, &Transaction{}, &Registration{})

	// Create Fresh Category Table
	if (db.Migrator().HasTable(&User{})) {
		fmt.Println("User table exist")
		db.Migrator().DropTable(&User{})
	}
	db.Migrator().CreateTable(&User{})

	// Create Fresh Book Table
	if (db.Migrator().HasTable(&Event{})) {
		fmt.Println("Event table exist")
		db.Migrator().DropTable(&Event{})
	}
	db.Migrator().CreateTable(&Event{})
	// Create Fresh Book Table
	if (db.Migrator().HasTable(&Transaction{})) {
		fmt.Println("Transaction table exist")
		db.Migrator().DropTable(&Transaction{})
	}
	db.Migrator().CreateTable(&Transaction{})

	// Create Fresh Registration Table
	if (db.Migrator().HasTable(&Registration{})) {
		fmt.Println("Transaction table exist")
		db.Migrator().DropTable(&Registration{})
	}
	db.Migrator().CreateTable(&Registration{})

}

// CRUD User

func GetAllUsers() ([]User, error) {
	// var users []User
	// result := db.Preload("Events").Find(&users)
	// return users, result.Error

	statement1 := "SELECT * FROM users"

	// SELECT * FROM events WHERE user_id IN (1,2,3,4);
	// `
	var users []User
	var finalUsers []User

	result1 := db.Raw(statement1).Scan(&users)
	// fmt.Println("result1: ", result1.Error)
	for _, user := range users {
		user.Events, _ = GetAllEventsByUserID(int(user.ID))
		user.Transactions, _ = GetAllTransactionsByUserID(int(user.ID))
		finalUsers = append(finalUsers, user)
	}
	return finalUsers, result1.Error
	// statement := "SELECT * FROM users LEFT JOIN events ON users.id = events.creator_id"

}

func GetUser(id int) (User, error) {
	statement := "SELECT * FROM users WHERE id=?"
	var user User
	result := db.Raw(statement, id).Scan(&user)
	user.Events, _ = GetAllEventsByUserID(int(user.ID))
	user.Transactions, _ = GetAllTransactionsByUserID(int(user.ID))
	return user, result.Error
}
func GetUserByEmail(email string) (User, error) {
	statement := "SELECT * FROM users WHERE email=?"
	var user User
	result := db.Raw(statement, email).Scan(&user)
	fmt.Println("user object: ", user)
	return user, result.Error
}
func GetUserByUsername(username string) (User, error) {
	statement := "SELECT * FROM users WHERE username=?"
	var user User
	result := db.Raw(statement, username).Scan(&user)
	fmt.Println("user object: ", user)
	return user, result.Error
}
func GetAllParticipants() ([]User, error) {
	statement1 := `SELECT * FROM users WHERE role=?`

	// SELECT * FROM events WHERE user_id IN (1,2,3,4);
	// `
	var users []User
	var finalUsers []User

	result1 := db.Raw(statement1, "participant").Scan(&users)
	// fmt.Println("result1: ", result1.Error)
	for _, user := range users {
		user.Events, _ = GetAllEventsByUserID(int(user.ID))
		user.Transactions, _ = GetAllTransactionsByUserID(int(user.ID))
		finalUsers = append(finalUsers, user)
	}
	return finalUsers, result1.Error
}

func DeleteAllUsers() ([]User, error) {
	statement := "DELETE FROM users RETURNING *"
	var allUsers []User
	result := db.Raw(statement).Scan(&allUsers)
	return allUsers, result.Error
}

func DeleteUser(id int) (User, error) {
	statement := "DELETE FROM users WHERE id=? RETURNING *"
	var deletedUser User
	result := db.Raw(statement, id).Scan(&deletedUser)
	return deletedUser, result.Error
}

func AddUser(user User) (User, error) {
	statement := "INSERT INTO users (username, fullname, email, password, role, deleted_at, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?) RETURNING *"

	var addedUser User
	result := db.Raw(statement, user.Username, user.Fullname, user.Email, user.Password, user.Role, user.Deleted_at, user.Created_at, user.Updated_at).Scan(&addedUser)
	return addedUser, result.Error

}

func UpdateUser(user User) (User, error) {
	statement := `UPDATE users SET 
							username=?, 
							fullname=?, 
							email=?, 
							password=?, 
							role=?, 
							deleted_at=?, 
							created_at=?, 
							updated_at=?
					WHERE id=?
					RETURNING *`

	var updatedUser User
	result := db.Raw(statement, user.Username, user.Fullname, user.Email, user.Password, user.Role, user.Deleted_at, user.Created_at, user.Updated_at, user.ID).Scan(&updatedUser)
	return updatedUser, result.Error

}

// CRUD Event

func GetAllEvents() ([]Event, error) {

	statement1 := "SELECT * FROM events"

	var events []Event
	var finalEvents []Event

	result := db.Raw(statement1).Scan(&events)

	for _, event := range events {
		event.Transactions, _ = GetAllTransactionsByEventID(int(event.ID))
		finalEvents = append(finalEvents, event)
	}

	return finalEvents, result.Error
}

func GetAllEventsByUserID(userID int) ([]Event, error) {
	statement1 := "SELECT * FROM events"

	var events []Event
	var finalEvents []Event
	result1 := db.Raw(statement1).Scan(&events)
	// fmt.Println("result1 GetAllEventsByUserID: ", result1.Error)

	for _, event := range events {
		event.Transactions, _ = GetAllTransactionsByEventID(int(event.ID))
		finalEvents = append(finalEvents, event)
	}

	return finalEvents, result1.Error
}

func GetEvent(id int) (Event, error) {
	statement := "SELECT * FROM events WHERE id=?"
	var event Event
	result := db.Raw(statement, id).Scan(&event)
	event.Transactions, _ = GetAllTransactionsByEventID(id)
	return event, result.Error
}

func GetTomorrowEvents() ([]Event, error) {
	statement := "SELECT * FROM events WHERE event_start_date>=? AND event_start_date<=?"
	var events []Event
	var finalEvents []Event
	//take tomorrow events which happen for the next 23-24 hours.
	result := db.Raw(statement, time.Now().Add(time.Hour*23), time.Now().Add(time.Hour*24)).Scan(&events)
	// fmt.Println("result1 GetAllEventsByUserID: ", result1.Error)

	for _, event := range events {
		event.Transactions, _ = GetAllTransactionsByEventID(int(event.ID))
		finalEvents = append(finalEvents, event)
	}
	return finalEvents, result.Error
}
func DeleteAllEvents() ([]Event, error) {
	statement := "DELETE FROM events RETURNING *"
	var allEvents []Event
	result := db.Raw(statement).Scan(&allEvents)
	return allEvents, result.Error
}

func DeleteEvent(id int) (Event, error) {
	statement := "DELETE FROM events WHERE id=? RETURNING *"
	var deletedEvent Event
	result := db.Raw(statement, id).Scan(&deletedEvent)
	return deletedEvent, result.Error
}

func AddEvent(event Event) (Event, error) {
	statement := "INSERT INTO events (creator_id, title_event, link_webinar, description, banner, price, quantity, status, event_start_date, event_end_date, campaign_start_date, campaign_end_date, deleted_at, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING *"

	var addedEvent Event
	result := db.Raw(statement, event.CreatorID, event.Title_event, event.Link_webinar, event.Description, event.Banner, event.Price, event.Quantity, event.Status, event.Event_start_date, event.Event_end_date, event.Campaign_start_date, event.Campaign_end_date, event.Deleted_at, event.Created_at, event.Updated_at).Scan(&addedEvent)

	return addedEvent, result.Error

}

func UpdateEvent(event Event) (Event, error) {
	statement := `UPDATE events SET
							creator_id=?, 
							title_event=?, 
							link_webinar=?, 
							description=?, 
							banner=?, 
							price=?, 
							quantity=?, 
							status=?, 
							event_start_date=?, 
							event_end_date=?, 
							campaign_start_date=?, 
							campaign_end_date=?, 
							deleted_at=?, 
							created_at=?, 
							updated_at=?
					WHERE id=?
					RETURNING *`

	var updatedEvent Event
	result := db.Raw(statement, event.CreatorID, event.Title_event, event.Link_webinar, event.Description, event.Banner, event.Price, event.Quantity, event.Status, event.Event_start_date, event.Event_end_date, event.Campaign_start_date, event.Campaign_end_date, event.Deleted_at, event.Created_at, event.Updated_at, event.ID).Scan(&updatedEvent)
	return updatedEvent, result.Error

}

// CRUD Transaction

func GetAllTransactions() ([]Transaction, error) {

	statement := "SELECT * FROM transactions"
	var transactions []Transaction
	result := db.Raw(statement).Scan(&transactions)
	return transactions, result.Error
}

func GetAllTransactionsByUserID(userID int) ([]Transaction, error) {
	statement := "SELECT * FROM transactions WHERE creator_id=?"

	var transactions []Transaction
	result := db.Raw(statement, userID).Scan(&transactions)
	// fmt.Println("result1 GetAllTransactionsByUserID: ", result.Error)

	return transactions, result.Error
}
func GetAllTransactionsByEventID(eventID int) ([]Transaction, error) {
	statement := "SELECT * FROM transactions WHERE event_id=?"

	var transactions []Transaction
	result := db.Raw(statement, eventID).Scan(&transactions)
	// fmt.Println("result1 GetAllTransactionsByEventID: ", result.Error)

	return transactions, result.Error
}

func GetTransaction(id int) (Transaction, error) {
	statement := "SELECT * FROM transactions WHERE id=?"
	var transaction Transaction
	result := db.Raw(statement, id).Scan(&transaction)
	return transaction, result.Error
}

func DeleteAllTransactions() ([]Transaction, error) {
	statement := "DELETE FROM transactions RETURNING *"
	var allTransactions []Transaction
	result := db.Raw(statement).Scan(&allTransactions)
	return allTransactions, result.Error
}

func DeleteTransaction(id int) (Transaction, error) {
	statement := "DELETE FROM transactions WHERE id=? RETURNING *"
	var deletedTransaction Transaction
	result := db.Raw(statement, id).Scan(&deletedTransaction)
	return deletedTransaction, result.Error
}

func AddTransaction(transaction Transaction) (Transaction, error) {
	statement := "INSERT INTO transactions (participant_id, creator_id, event_id, amount, status_payment, deleted_at, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?) RETURNING *"

	var addedTransaction Transaction
	result := db.Raw(statement, transaction.ParticipantID, transaction.CreatorID, transaction.EventID, transaction.Amount, transaction.Status_payment, transaction.Deleted_at, transaction.Created_at, transaction.Updated_at).Scan(&addedTransaction)

	return addedTransaction, result.Error

}

func UpdateTransaction(transaction Transaction) (Transaction, error) {
	statement := `UPDATE transactions SET
							participant_id=?,
							creator_id=?,
							event_id=?,
							amount=?,
							status_payment=?,
							deleted_at=?,
							created_at=?,
							updated_at=?
					WHERE id=?
					RETURNING *`

	var updatedTransaction Transaction
	result := db.Raw(statement, transaction.ParticipantID, transaction.CreatorID, transaction.EventID, transaction.Amount, transaction.Status_payment, transaction.Deleted_at, transaction.Created_at, transaction.Updated_at, transaction.ID).Scan(&updatedTransaction)
	return updatedTransaction, result.Error

}

// CRUD Registration

func GetAllRegistrations() ([]Registration, error) {

	statement1 := "SELECT * FROM registrations"

	var registrations []Registration
	result := db.Raw(statement1).Scan(&registrations)
	return registrations, result.Error
}

func GetAllRegistrationsByUserID(userID int) ([]Registration, error) {
	statement1 := "SELECT * FROM registrations WHERE user_id=?"

	var registrations []Registration
	result1 := db.Raw(statement1, userID).Scan(&registrations)
	// fmt.Println("result1 GetAllRegistrationsByUserID: ", result1.Error)

	return registrations, result1.Error
}

func GetRegistration(id int) (Registration, error) {
	statement := "SELECT * FROM registrations WHERE id=?"
	var registration Registration
	result := db.Raw(statement, id).Scan(&registration)
	return registration, result.Error
}

func DeleteAllRegistrations() ([]Registration, error) {
	statement := "DELETE FROM registrations RETURNING *"
	var allRegistrations []Registration
	result := db.Raw(statement).Scan(&allRegistrations)
	return allRegistrations, result.Error
}

func DeleteRegistration(id int) (Registration, error) {
	statement := "DELETE FROM registrations WHERE id=? RETURNING *"
	var deletedRegistration Registration
	result := db.Raw(statement, id).Scan(&deletedRegistration)
	return deletedRegistration, result.Error
}

func AddRegistration(registration Registration) (Registration, error) {
	statement := "INSERT INTO registrations (registration_token, time_created, user_id, deleted_at, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?) RETURNING *"

	var addedRegistration Registration
	result := db.Raw(statement, registration.RegistrationToken, registration.TimeCreated, registration.UserID, time.Now(), time.Now(), time.Now()).Scan(&addedRegistration)

	return addedRegistration, result.Error

}

// REPORT

func GetEventReport(eventID int) (Report, error) {
	// TotalParticipant int
	// DetailWebinar    Event
	// ParticipantLists []User
	// TotalMoney       int64
	detailWebinar, err := GetEvent(eventID)
	if err != nil {
		return Report{}, err
	}
	totalParticipant := len(detailWebinar.Transactions)
	transactions := detailWebinar.Transactions
	var participantLists []User
	var totalMoney int64
	for _, transaction := range transactions {
		userID := transaction.ParticipantID
		user, err := GetUser(userID)
		if err != nil {
			return Report{}, err
		}
		participantLists = append(participantLists, user)
		totalMoney += transaction.Amount
	}
	var report = Report{
		TotalParticipant: totalParticipant,
		DetailWebinar:    detailWebinar,
		ParticipantLists: participantLists,
		TotalMoney:       totalMoney,
	}
	return report, nil

}

func GetEventParticipantReport(id int) ([]User, error) {
	report, err := GetEventReport(id)
	return report.ParticipantLists, err
}

func GetEventReportSortByMoney(creatorID int) ([]Report, error) {
	events, err := GetAllEventsByUserID(creatorID)
	if err != nil {
		return []Report{}, err
	}

	var reports []Report
	for _, event := range events {
		report, err := GetEventReport(int(event.ID))
		if err != nil {
			return []Report{}, err
		}
		reports = append(reports, report)
	}
	sort.SliceStable(reports, func(i, j int) bool {
		return reports[i].TotalMoney > reports[j].TotalMoney
	})
	return reports, nil
}

func GetEventReportSortByParticipant(creatorID int) ([]Report, error) {
	events, err := GetAllEventsByUserID(creatorID)
	if err != nil {
		return []Report{}, err
	}

	var reports []Report
	for _, event := range events {
		report, err := GetEventReport(int(event.ID))
		if err != nil {
			return []Report{}, err
		}
		reports = append(reports, report)
	}
	sort.SliceStable(reports, func(i, j int) bool {
		return reports[i].TotalParticipant > reports[j].TotalParticipant
	})
	return reports, nil
}

// Order Ticket

func OrderTicket(eventID int, userID int) (Transaction, error) {
	event, err := GetEvent(eventID)
	if err != nil {
		return Transaction{}, err
	}
	totalPrice := event.Price
	var transaction = Transaction{
		ParticipantID:  userID,
		EventID:        uint(eventID),
		Amount:         totalPrice,
		Status_payment: "failed",
	}
	addedTransaction, err := AddTransaction(transaction)
	return addedTransaction, err
}

func OrderTicketCheckout(eventID int, userID int) error {
	//Send Confirmation Email
	// random := randstr.Hex(16) // generate 128-bit hex string
	// user, _ := GetUser(userID)
	event, err := GetEvent(eventID)
	if err != nil {
		return err
	}
	msg := []byte("To: oceankingdigital@gmail.com\r\n" +
		"Subject: Order Confirmation Email from Ticketing App !\r\n" +
		"\r\n" +
		"\r\nEvent Title:\r\n" + event.Title_event +
		"\r\nEvent Description:\r\n" + event.Description +
		"\r\n" +
		"Silakan lakukan pembayaran ke rekening berikut ini: \r\n" +
		"Bank Mandiri:12345677 a.n. PT.Refactory \r\n" +
		"Bank BCA:12345677 a.n. PT.Refactory \r\n" +
		"Bank BNI:12345677 a.n. PT.Refactory \r\n" +
		"Bank BRI:12345677 a.n. PT.Refactory \r\n" +
		"r\n" +
		"Setelah melakukan pembayaran silakan upload foto bukti transfer ke halaman berikut:r\n" +
		"http://localhost:8080/events/bukti-transfer")

	toEmail := []string{"oceankingdigital@gmail.com"}
	email.SendEmail(toEmail, msg)
	// from := "Refactory Ticketing App <pranotobudi.app@gmail.com>"
	// to := "oceankingdigital@gmail.com"
	// subject := "Order Confirmation Email from Ticketing App !"
	// message := `
	// <b>Event Title:</b>` + event.Title_event + `</br>` +
	// 	`<b>Event Description:</b>` + event.Description + `</br>` +
	// 	`Silakan lakukan pembayaran ke rekening berikut ini: </br>
	// 	Bank Mandiri:12345677 a.n. PT.Refactory </br>
	// 	Bank BCA:12345677 a.n. PT.Refactory </br>
	// 	Bank BNI:12345677 a.n. PT.Refactory </br>
	// 	Bank BRI:12345677 a.n. PT.Refactory </br>
	// 	</br>
	// 	Setelah melakukan pembayaran silakan upload foto bukti transfer ke halaman berikut:</br>
	// 	http://localhost:8080/events/bukti-transfer
	// `
	// email.SendMailByEmailLib(from, to, subject, message)
	return nil
}
func OrderTicketConfirmation(eventID int, userID int, transactionID int) (Transaction, error) {
	//Send Confirmation Email
	// random := randstr.Hex(16) // generate 128-bit hex string
	// user, _ := GetUser(userID)
	transaction, err := GetTransaction(transactionID)
	if err != nil {
		return Transaction{}, err
	}
	transaction.Status_payment = "passed"
	updatedTransaction, err := UpdateTransaction(transaction)
	if err != nil {
		return Transaction{}, err
	}
	event, err := GetEvent(eventID)
	if err != nil {
		return Transaction{}, err
	}
	msg := []byte("To: oceankingdigital@gmail.com\r\n" +
		"Subject: Payment Confirmation Email from Ticketing App !\r\n" +
		"\r\n" +
		"Pembayaran Anda telah kami terima, berikut ini link webinar: \r\n" +
		"\r\nEvent Title:\r\n" + event.Title_event +
		"\r\nEvent Description:\r\n" + event.Description +
		"Event Link:\r\n" + event.Link_webinar +
		"\r\n")

	toEmail := []string{"oceankingdigital@gmail.com"}
	email.SendEmail(toEmail, msg)

	return updatedTransaction, nil
}

// func SendEmail(toAddress []string, message []byte) error {
// 	// Message.
// 	//   message := []byte("This is a test email message.")
// 	conf := config.New()
// 	// Authentication.
// 	auth := smtp.PlainAuth("", conf.Email.FromAddress, conf.Email.FromEmailPassword, conf.Email.SmtpServer)

// 	// Sending email.
// 	err := smtp.SendMail(conf.Email.SmtpServer+":"+fmt.Sprint(conf.Email.SmtpPort), auth, conf.Email.FromAddress, toAddress, message)
// 	if err != nil {
// 		fmt.Println(err)
// 		return err
// 	}
// 	fmt.Println("Email Sent Successfully!")
// 	return nil
// }

func AddRegistrationByUserID(token string, userID int) (Registration, error) {
	registration := Registration{
		RegistrationToken: token,
		TimeCreated:       time.Now(),
		UserID:            uint(userID),
	}
	addedRegistration, err := AddRegistration(registration)
	return addedRegistration, err
}

func IsValidUserData(user User) bool {
	return true
}

func UserRegister(user User) (Registration, error) {
	//Send Confirmation Email
	random := randstr.Hex(16) // generate 128-bit hex string
	addedUser, _ := AddUser(user)
	addedRegistration, err := AddRegistrationByUserID(random, int(addedUser.ID))
	if err != nil {
		return Registration{}, err
	}

	msg := []byte("To: oceankingdigital@gmail.com\r\n" +
		"Subject: Confirmation Email from Ticketing App!\r\n" +
		"\r\n" +
		"This is the email body.\r\n" +
		"http://localhost:8080/register/confirmation?email=" + user.Email + "&token=" + random)

	toEmail := []string{"oceankingdigital@gmail.com"}
	err = email.SendEmail(toEmail, msg)
	if err != nil {
		return addedRegistration, err
	}
	return addedRegistration, nil
}
