package database

// func OpenConnection(driverName, dataSource string) (*DBType, error) {
// 	db, err := sql.Open(driverName, dataSource)
// 	if err != nil {
// 		return nil, err
// 	}
// 	// var dbType DBType
// 	// dbType.db = db
// 	// return &dbType, nil
// 	// defer db.Close()
// 	db.Ping()
// 	return &DBType{db}, nil
// }

// func (dbPointer *DBType) CreateDB(dbname string) error {
// 	_, err := dbPointer.DB.Exec("CREATE DATABASE " + dbname)
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }

// func (dbPointer *DBType) DropDBIfExist(dbname string) error {
// 	_, err := dbPointer.DB.Exec("DROP DATABASE IF EXISTS " + dbname)
// 	if err != nil {
// 		// panic(err)
// 		return err
// 	}
// 	return dbPointer.CreateDB(dbname)
// }

// func (dbPointer *DBType) CreatePhoneNumberTable() error {
// 	statement := `
// 		CREATE TABLE IF NOT EXISTS phone_number(
// 			id SERIAL,
// 			value VARCHAR(255)
// 		)
// 	`
// 	_, err := dbPointer.DB.Exec(statement)
// 	return err
// }

// func InsertUser(db *sql.DB) error {
// 	statement := `INSERT INTO users(value) VALUES("first")`
// 	rows := db.QueryRow(statement)
// 	if rows.Err() != nil {
// 		return rows.Err()
// 	}
// 	return nil
// }
// func InitTable(db *gorm.DB) error {
// 	db.AutoMigrate(&User{})
// 	// Create Fresh Category Table
// 	if (db.Migrator().HasTable(&User{})) {
// 		fmt.Println("Category table exist")
// 		db.Migrator().DropTable(&User{})
// 	}
// 	db.Migrator().CreateTable(&User{})
// 	return nil
// }

// func CreateUserTable(db *sql.DB) error {
// 	statement := `
// 	CREATE TABLE IF NOT EXISTS users(
// 		id SERIAL,
// 		value VARCHAR(255)
// 	)
// 	`

// 	_, err := db.Exec(statement)
// 	if err != nil {
// 		fmt.Println(err.Error())
// 	} else {
// 		fmt.Println("Table created successfully..")
// 	}

// 	// query := `CREATE TABLE IF NOT EXISTS product(product_id int primary key auto_increment, product_name text,
// 	//     product_price int, created_at datetime default CURRENT_TIMESTAMP, updated_at datetime default CURRENT_TIMESTAMP)`

// 	// _, err := db.Exec(query)
// 	// fmt.Println("rows affected")
// 	// // return err
// 	// if err != nil {
// 	// 	return err
// 	// }
// 	return nil
// }

//TerminateConnection disconnect everything except your session from the database you are connected to
// func (dbPointer *DBType) TerminateConnectionExceptThis(dbname string) {
// 	statement := fmt.Sprintf(`
// 	SELECT pg_terminate_backend(pg_stat_activity.pid)
//     FROM pg_stat_activity
//     WHERE pg_stat_activity.datname = '%s'
//       AND pid <> pg_backend_pid();
// 	`, dbname)

// 	_, err := dbPointer.DB.Exec(statement)
// 	if err != nil {
// 		panic(err)
// 	}

// }
