package dto

import "time"

type UserDTO struct {
	ID       string `json:"id" form:"id"`
	Username string `json:"username" form:"username" binding:"required"`
	Fullname string `json:"fullname" form:"fullname"`
	Email    string `json:"email" form:"email" binding:"required"`
	Password string `json:"password" form:"password" binding:"required"`
	Role     string `json:"role" form:"role" binding:"required"`
}

type EventDTO struct {
	CreatorID           uint      `json:"creator_id" form:"creator_id"`
	Title_event         string    `json:"title_event" form:"title_event"`
	Link_webinar        string    `json:"link_webinar" form:"link_webinar"`
	Description         string    `json:"description" form:"description"`
	Banner              string    `json:"banner" form:"banner"`
	Price               int64     `json:"price" form:"price"`
	Quantity            int       `json:"quantity" form:"quantity"`
	Status              string    `json:"status" form:"status"`
	Event_start_date    time.Time `json:"event_start_date" form:"event_start_date"`
	Event_end_date      time.Time `json:"event_end_date" form:"event_end_date"`
	Campaign_start_date time.Time `json:"campaign_start_date" form:"campaign_start_date"`
	Campaign_end_date   time.Time `json:"campaign_end_date" form:"campaign_end_date"`
}
