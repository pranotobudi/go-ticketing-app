package router

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/pranotobudi/go-ticketing-app/database"
	"gitlab.com/pranotobudi/go-ticketing-app/database/dto"
	"gitlab.com/pranotobudi/go-ticketing-app/redis"
	"gitlab.com/pranotobudi/go-ticketing-app/tasks"
)

func GetAllEvents(c *gin.Context) {
	// r.GET("/events", router.GetAllEvents)
	events, err := database.GetAllEvents()
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	booksMarshal, _ := json.Marshal(events)
	err = redis.SetKeyValue(c.Request.URL.Path, booksMarshal)
	if err != nil {
		c.JSON(http.StatusInternalServerError, "failed to set redis value: "+err.Error())
	}

	c.JSON(http.StatusOK, events)

}

func GetEvent(c *gin.Context) {
	// r.GET("/events/:id", router.GetEvent)
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	fmt.Println("ListUser ERROR: ", err)

	event, err := database.GetEvent(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, event)

}

func DeleteAllEvents(c *gin.Context) {
	// r.DELETE("/events", router.DeleteAllEvents)
	events, err := database.DeleteAllEvents()
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Delete All Event Success")
	c.JSON(http.StatusOK, events)
}

func DeleteEvent(c *gin.Context) {
	// r.DELETE("/events/:id", router.DeleteEvent)

	idStr := c.Param("id")
	id, _ := strconv.Atoi(idStr)

	deletedEvent, err := database.DeleteEvent(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Delete Event Success, id: "+idStr)
	c.JSON(http.StatusOK, deletedEvent)
}

func AddEventJSON(c *gin.Context) {
	var eventDTO dto.EventDTO
	if err := c.ShouldBindJSON(&eventDTO); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	event := database.Event{
		CreatorID:           eventDTO.CreatorID,
		Title_event:         eventDTO.Title_event,
		Link_webinar:        eventDTO.Link_webinar,
		Description:         eventDTO.Description,
		Banner:              eventDTO.Banner,
		Price:               eventDTO.Price,
		Quantity:            eventDTO.Quantity,
		Status:              eventDTO.Status,
		Event_start_date:    eventDTO.Event_start_date,
		Event_end_date:      eventDTO.Event_end_date,
		Campaign_start_date: eventDTO.Campaign_start_date,
		Campaign_end_date:   eventDTO.Campaign_end_date,
		Deleted_at:          time.Now(),
		Created_at:          time.Now(),
		Updated_at:          time.Now(),
	}
	addedEvent, err := database.AddEvent(event)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	tasks.AddEventQueue(addedEvent)
	c.JSON(http.StatusOK, "Add Book Success")
	c.JSON(http.StatusOK, addedEvent)

}
func AddEvent(c *gin.Context) {

	// r.POST("/events", router.AddEvent)
	var layoutFormat = "2006-01-02 15:04:05"

	creator_id, _ := strconv.Atoi(c.PostForm("creator_id"))
	title_event := c.PostForm("title_event")
	link_webinar := c.PostForm("link_webinar")
	description := c.PostForm("description")
	banner := c.PostForm("banner")
	price, _ := strconv.Atoi(c.PostForm("price"))
	quantity, _ := strconv.Atoi(c.PostForm("quantity"))
	status := c.PostForm("status")
	event_start_date, _ := time.Parse(layoutFormat, c.PostForm("event_start_date"))
	event_end_date, _ := time.Parse(layoutFormat, c.PostForm("event_end_date"))
	campaign_start_date, _ := time.Parse(layoutFormat, c.PostForm("campaign_start_date"))
	campaign_end_date, _ := time.Parse(layoutFormat, c.PostForm("campaign_end_date"))

	event := database.Event{
		CreatorID:           uint(creator_id),
		Title_event:         title_event,
		Link_webinar:        link_webinar,
		Description:         description,
		Banner:              banner,
		Price:               int64(price),
		Quantity:            quantity,
		Status:              status,
		Event_start_date:    event_start_date,
		Event_end_date:      event_end_date,
		Campaign_start_date: campaign_start_date,
		Campaign_end_date:   campaign_end_date,
		Deleted_at:          time.Now(),
		Created_at:          time.Now(),
		Updated_at:          time.Now(),
	}
	addedEvent, err := database.AddEvent(event)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	tasks.AddEventQueue(addedEvent)
	c.JSON(http.StatusOK, "Add Book Success")
	c.JSON(http.StatusOK, addedEvent)
}

func UpdateEvent(c *gin.Context) {
	// r.PUT("/events/:id", router.UpdateEvent)
	idStr := c.Param("id")
	id, _ := strconv.Atoi(idStr)
	var layoutFormat = "2006-01-02 15:04:05"

	creator_id, _ := strconv.Atoi(c.PostForm("creator_id"))
	title_event := c.PostForm("title_event")
	link_webinar := c.PostForm("link_webinar")
	description := c.PostForm("description")
	banner := c.PostForm("banner")
	price, _ := strconv.Atoi(c.PostForm("price"))
	quantity, _ := strconv.Atoi(c.PostForm("quantity"))
	status := c.PostForm("status")
	event_start_date, _ := time.Parse(layoutFormat, c.PostForm("event_start_date"))
	event_end_date, _ := time.Parse(layoutFormat, c.PostForm("event_end_date"))
	campaign_start_date, _ := time.Parse(layoutFormat, c.PostForm("campaign_start_date"))
	campaign_end_date, _ := time.Parse(layoutFormat, c.PostForm("campaign_end_date"))

	event, _ := database.GetEvent(id)
	event = database.Event{
		ID:                  event.ID,
		CreatorID:           uint(creator_id),
		Title_event:         title_event,
		Link_webinar:        link_webinar,
		Description:         description,
		Banner:              banner,
		Price:               int64(price),
		Quantity:            quantity,
		Status:              status,
		Event_start_date:    event_start_date,
		Event_end_date:      event_end_date,
		Campaign_start_date: campaign_start_date,
		Campaign_end_date:   campaign_end_date,
		Deleted_at:          time.Now(),
		Created_at:          time.Now(),
		Updated_at:          time.Now(),
	}

	updatedEvent, err := database.UpdateEvent(event)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Update Event Success id: "+idStr)
	c.JSON(http.StatusOK, updatedEvent)
}

func GetEventReport(c *gin.Context) {
	eventIDStr := c.Param("id")
	eventID, _ := strconv.Atoi(eventIDStr)

	doneEventReport, err := database.GetEventReport(eventID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Get Done Event Success event id: "+eventIDStr)
	c.JSON(http.StatusOK, doneEventReport)

}

func GetEventParticipantReport(c *gin.Context) {
	eventIDStr := c.Param("id")
	eventID, _ := strconv.Atoi(eventIDStr)

	eventParticipantReport, err := database.GetEventParticipantReport(eventID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Get Event Participant Success id: "+eventIDStr)
	c.JSON(http.StatusOK, eventParticipantReport)

}

func GetEventReportSortByMoney(c *gin.Context) {
	creatorIDStr := c.Param("creator_id")
	creatorID, _ := strconv.Atoi(creatorIDStr)

	eventReportsSortByMoney, err := database.GetEventReportSortByMoney(creatorID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	booksMarshal, _ := json.Marshal(eventReportsSortByMoney)
	err = redis.SetKeyValue(c.Request.URL.Path, booksMarshal)
	if err != nil {
		c.JSON(http.StatusInternalServerError, "failed to set redis value: "+err.Error())
	}

	c.JSON(http.StatusOK, "Get Event Report Sort By Money Success")
	c.JSON(http.StatusOK, eventReportsSortByMoney)
}
func GetEventReportSortByParticipant(c *gin.Context) {
	creatorIDStr := c.Param("creator_id")
	creatorID, _ := strconv.Atoi(creatorIDStr)

	eventReportsSortByParticipant, err := database.GetEventReportSortByParticipant(creatorID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	booksMarshal, _ := json.Marshal(eventReportsSortByParticipant)
	err = redis.SetKeyValue(c.Request.URL.Path, booksMarshal)
	if err != nil {
		c.JSON(http.StatusInternalServerError, "failed to set redis value: "+err.Error())
	}

	c.JSON(http.StatusOK, "Get Event Report Sort By Participant Success")
	c.JSON(http.StatusOK, eventReportsSortByParticipant)
}
