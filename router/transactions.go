package router

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/pranotobudi/go-ticketing-app/config"
	"gitlab.com/pranotobudi/go-ticketing-app/database"
	"gitlab.com/pranotobudi/go-ticketing-app/redis"
)

func GetAllTransactions(c *gin.Context) {
	// r.GET("/transactions", router.GetAllTransactions)
	transactions, err := database.GetAllTransactions()
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	booksMarshal, _ := json.Marshal(transactions)
	err = redis.SetKeyValue(c.Request.URL.Path, booksMarshal)
	if err != nil {
		c.JSON(http.StatusInternalServerError, "failed to set redis value: "+err.Error())
	}

	c.JSON(http.StatusOK, transactions)

}

func GetTransaction(c *gin.Context) {
	// r.GET("/transactions/:id", router.GetTransaction)
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	fmt.Println("ListUser ERROR: ", err)

	transaction, err := database.GetTransaction(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, transaction)

}

func DeleteAllTransactions(c *gin.Context) {
	// r.DELETE("/transactions", router.DeleteAllTransactions)
	transactions, err := database.DeleteAllTransactions()
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	err = redis.DelKey(c.Request.URL.Path)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, "Delete All Transactions Success")
	c.JSON(http.StatusOK, transactions)
}

func DeleteTransaction(c *gin.Context) {
	// r.DELETE("/transactions/:id", router.DeleteTransaction)

	idStr := c.Param("id")
	id, _ := strconv.Atoi(idStr)

	deletedTransaction, err := database.DeleteTransaction(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Delete Book Success, id: "+idStr)
	c.JSON(http.StatusOK, deletedTransaction)
}

func AddTransaction(c *gin.Context) {

	// r.POST("/transactions", router.AddTransaction)
	participant_id, _ := strconv.Atoi(c.PostForm("participant_id"))
	creator_id, _ := strconv.Atoi(c.PostForm("creator_id"))
	event_id, _ := strconv.Atoi(c.PostForm("event_id"))
	amount, _ := strconv.Atoi(c.PostForm("amount"))
	status_payment := c.PostForm("status_payment")
	transaction := database.Transaction{
		ParticipantID:  participant_id,
		CreatorID:      creator_id,
		EventID:        uint(event_id),
		Amount:         int64(amount),
		Status_payment: status_payment,
		Deleted_at:     time.Now(),
		Created_at:     time.Now(),
		Updated_at:     time.Now(),
	}
	addedTransaction, err := database.AddTransaction(transaction)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Add Transaction Success")
	c.JSON(http.StatusOK, addedTransaction)
}

func UpdateTransaction(c *gin.Context) {
	// r.PUT("/transactions/:id", router.UpdateTransaction)
	idStr := c.Param("id")
	id, _ := strconv.Atoi(idStr)
	participant_id, _ := strconv.Atoi(c.PostForm("participant_id"))
	creator_id, _ := strconv.Atoi(c.PostForm("creator_id"))
	event_id, _ := strconv.Atoi(c.PostForm("event_id"))
	amount, _ := strconv.Atoi(c.PostForm("amount"))
	status_payment := c.PostForm("status_payment")
	transaction, _ := database.GetTransaction(id)
	transaction = database.Transaction{
		ID:             transaction.ID,
		ParticipantID:  participant_id,
		CreatorID:      creator_id,
		EventID:        uint(event_id),
		Amount:         int64(amount),
		Status_payment: status_payment,
		Deleted_at:     time.Now(),
		Created_at:     time.Now(),
		Updated_at:     time.Now(),
	}

	updatedTransaction, err := database.UpdateTransaction(transaction)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Update Book Success id: "+idStr)
	c.JSON(http.StatusOK, updatedTransaction)
}

func OrderTicket(c *gin.Context) {
	eventIDStr := c.Param("id")
	eventID, _ := strconv.Atoi(eventIDStr)
	userID, _ := strconv.Atoi(c.PostForm("user_id"))

	createdTransaction, err := database.OrderTicket(eventID, userID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Order Ticket Success id: "+eventIDStr)
	c.JSON(http.StatusOK, createdTransaction)

}

func OrderTicketCheckout(c *gin.Context) {
	eventIDStr := c.Param("id")
	eventID, _ := strconv.Atoi(eventIDStr)
	userID, _ := strconv.Atoi(c.PostForm("user_id"))

	err := database.OrderTicketCheckout(eventID, userID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Email Sent Successfully")
	// c.JSON(http.StatusOK, createdTransaction)

}

func OrderTicketConfirmation(c *gin.Context) {
	eventIDStr := c.Param("id")
	eventID, _ := strconv.Atoi(eventIDStr)
	userID, _ := strconv.Atoi(c.PostForm("user_id"))
	transactionID, _ := strconv.Atoi(c.PostForm("transaction_id"))

	updatedTransaction, err := database.OrderTicketConfirmation(eventID, userID, transactionID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Email Sent Successfully, updatedTransaction: ")
	c.JSON(http.StatusOK, updatedTransaction)

}

func OrderTicketBuktiTransferUpload(c *gin.Context) {
	conf := config.New()
	if c.Request.ContentLength > int64(conf.File.MaxFileSize) {
		c.JSON(http.StatusExpectationFailed, fmt.Sprintf("request too large, file size Content length: %d, maximum: %d", c.Request.ContentLength, conf.File.MaxFileSize))
		return
	}
	c.Request.Body = http.MaxBytesReader(c.Writer, c.Request.Body, int64(conf.File.MaxFileSize))
	// err := c.Request.ParseMultipartForm(int64(conf.File.MaxFileSize))

	err := c.Request.ParseMultipartForm(int64(conf.File.MaxFileSize))
	if err != nil {
		c.JSON(http.StatusInternalServerError, fmt.Sprintf("parseMutlipartForm failed: %s", err.Error()))
		return
	}
	file, handler, err := c.Request.FormFile("upload_file")
	if err != nil {
		c.JSON(http.StatusInternalServerError, fmt.Sprintf("failed to upload file: %s", err.Error()))
		return
	}
	defer file.Close()
	// fmt.Fprintf(c.Writer, "Handler ---- %v", handler.Header)
	f, err := os.OpenFile("./upload/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()
	io.Copy(f, file)
	c.JSON(http.StatusOK, "file upload success..!")
}
