package router

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/pranotobudi/go-ticketing-app/database"
	"gitlab.com/pranotobudi/go-ticketing-app/database/dto"
	"gitlab.com/pranotobudi/go-ticketing-app/redis"
)

func GetAllUsers(c *gin.Context) {
	// r.GET("/users", router.GetAllUsers)
	users, err := database.GetAllUsers()
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	usersMarshal, _ := json.Marshal(users)
	err = redis.SetKeyValue(c.Request.URL.Path, usersMarshal)
	if err != nil {
		c.JSON(http.StatusInternalServerError, "failed to set redis value: "+err.Error())
	}
	c.JSON(http.StatusOK, users)

}

func GetUser(c *gin.Context) {

	// r.GET("/users/:id", router.GetUser)
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	fmt.Println("ListUser ERROR: ", err)

	user, err := database.GetUser(id)
	userMarshal, _ := json.Marshal(user)
	err = redis.SetKeyValue(c.Request.URL.Path, userMarshal)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "function GetUser")
	c.JSON(http.StatusOK, user)
}

func GetAllParticipants(c *gin.Context) {
	// r.GET("/users", router.GetAllUsers)
	users, err := database.GetAllParticipants()
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	usersMarshal, _ := json.Marshal(users)
	err = redis.SetKeyValue(c.Request.URL.Path, usersMarshal)
	if err != nil {
		c.JSON(http.StatusInternalServerError, "failed to set redis value: "+err.Error())
	}
	c.JSON(http.StatusOK, users)

}
func DeleteAllUsers(c *gin.Context) {
	// r.DELETE("/users", router.DeleteAllUsers)
	users, err := database.DeleteAllUsers()
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, "Delete All Users Success")
	c.JSON(http.StatusOK, users)
}

func DeleteUser(c *gin.Context) {
	// r.DELETE("/users/:id", router.DeleteUser)

	idStr := c.Param("id")
	id, _ := strconv.Atoi(idStr)

	deletedUser, err := database.DeleteUser(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	err = redis.DelKey(c.Request.URL.Path)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, "Delete User Success, id: "+idStr)
	c.JSON(http.StatusOK, deletedUser)
}

func AddUserJSON(c *gin.Context) {
	var userDTO dto.UserDTO
	if err := c.ShouldBindJSON(&userDTO); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	user := database.User{
		Username:   userDTO.Username,
		Fullname:   userDTO.Fullname,
		Email:      userDTO.Email,
		Password:   userDTO.Password,
		Role:       userDTO.Role,
		Deleted_at: time.Now(),
		Created_at: time.Now(),
		Updated_at: time.Now(),
	}
	addedUser, err := database.AddUser(user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	userMarshal, _ := json.Marshal(user)
	err = redis.SetKeyValue(c.Request.URL.Path, userMarshal)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Add Book Success")
	c.JSON(http.StatusOK, addedUser)

}
func AddUser(c *gin.Context) {

	// r.POST("/users", router.AddUser)
	username := c.PostForm("username")
	fullname := c.PostForm("fullname")
	email := c.PostForm("email")
	password := c.PostForm("password")
	role := c.PostForm("role")

	user := database.User{
		Username:   username,
		Fullname:   fullname,
		Email:      email,
		Password:   password,
		Role:       role,
		Deleted_at: time.Now(),
		Created_at: time.Now(),
		Updated_at: time.Now(),
	}
	addedUser, err := database.AddUser(user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Add Book Success")
	c.JSON(http.StatusOK, addedUser)
}

func UpdateUserJSON(c *gin.Context) {
	// r.PUT("/users/:id", router.UpdateUser)
	idStr := c.Param("id")
	id, _ := strconv.Atoi(idStr)
	username := c.PostForm("username")
	fullname := c.PostForm("fullname")
	email := c.PostForm("email")
	password := c.PostForm("password")
	role := c.PostForm("role")

	user, err := database.GetUser(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
	}
	user = database.User{
		ID:         user.ID,
		Username:   username,
		Fullname:   fullname,
		Email:      email,
		Password:   password,
		Role:       role,
		Deleted_at: time.Now(),
		Created_at: time.Now(),
		Updated_at: time.Now(),
	}

	updatedUser, err := database.UpdateUser(user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Update Book Success id: "+idStr)
	c.JSON(http.StatusOK, updatedUser)
}
func UpdateUser(c *gin.Context) {
	// r.PUT("/users/:id", router.UpdateUser)
	idStr := c.Param("id")
	id, _ := strconv.Atoi(idStr)
	username := c.PostForm("username")
	fullname := c.PostForm("fullname")
	email := c.PostForm("email")
	password := c.PostForm("password")
	role := c.PostForm("role")

	user, err := database.GetUser(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
	}
	user = database.User{
		ID:         user.ID,
		Username:   username,
		Fullname:   fullname,
		Email:      email,
		Password:   password,
		Role:       role,
		Deleted_at: time.Now(),
		Created_at: time.Now(),
		Updated_at: time.Now(),
	}

	updatedUser, err := database.UpdateUser(user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, "Update Book Success id: "+idStr)
	c.JSON(http.StatusOK, updatedUser)
}

func UserRegisterJSON(c *gin.Context) {
	var user database.User
	c.BindJSON(&user)
	addedRegistration, err := database.UserRegister(user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, addedRegistration)
		return
	}
	// fmt.Println("USER: ", user)
	c.JSON(http.StatusOK, addedRegistration)

}

func EmailConfirmation(c *gin.Context) {
	query := c.Request.URL.Query()
	email := query["email"]
	token := query["token"]
	c.JSON(http.StatusOK, gin.H{"email": email, "token": token, "message": "Registration Success"})
	// user, _ := database.GetRegistration(email[0], token[0])
	// c.JSON(http.StatusOK, user)
}

func UserLogin(c *gin.Context) {
	username := c.PostForm("username")
	password := c.PostForm("password")
	if IsMember(username, password) {
		user, _ := database.GetUserByUsername(username)
		fmt.Printf("USERLOGIN user: %v role: %v \n", user, user.Role)
		token, _ := GenerateJWT(username, user.Role)
		c.JSON(http.StatusOK, gin.H{"jwt_token": token})
	} else {
		c.JSON(http.StatusUnauthorized, gin.H{"message": "Credential Not Found, Please Login First"})
	}
}
