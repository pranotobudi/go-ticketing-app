package router

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func V1(c *gin.Context) {
	c.JSON(http.StatusOK, "V1")
}

func V1Submit(c *gin.Context) {
	c.JSON(http.StatusOK, "V1 Submit")
}

func GlobalMiddleware(c *gin.Context) {
	fmt.Println("Global Middlware")
	c.Next()
	c.JSON(http.StatusOK, "Global Middlware")
}

func LocalMiddleware(c *gin.Context) {
	fmt.Println("Local Middlware 1")
	c.Abort()

	fmt.Println("Local Middlware 2")
	c.JSON(http.StatusOK, "Local Middlware")
}

func GroupMiddleware(c *gin.Context) {
	fmt.Println("Group Middlware")
	c.JSON(http.StatusOK, "Group Middlware")
}
