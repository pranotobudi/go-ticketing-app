package router

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/pranotobudi/go-ticketing-app/database"
)

var APPLICATION_NAME = "Rental Buku App"
var LOGIN_EXPIRATION_DURATION = time.Duration(24) * time.Hour
var JWT_SIGNING_METHOD = jwt.SigningMethodHS256
var JWT_SIGNATURE_KEY = []byte("the_secret_key")

type RoleAccess struct {
	Path        string
	AllowedRole string
}

// var RoleAccessList = []RoleAccess{
// 	{"/", "admin"},
// 	{"/", "creator"},
// 	{"/", "participant"},
// 	{"/users", "admin"},
// }

type MyClaims struct {
	jwt.StandardClaims
	Username string `json:"username"`
	Role     string `json:"role"`
}

func GenerateJWT(username string, role string) (string, error) {

	claims := MyClaims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    APPLICATION_NAME,
			ExpiresAt: time.Now().Add(LOGIN_EXPIRATION_DURATION).Unix(),
		},
		Username: username,
		Role:     role,
	}

	token := jwt.NewWithClaims(
		JWT_SIGNING_METHOD,
		claims,
	)
	signedToken, err := token.SignedString(JWT_SIGNATURE_KEY)
	if err != nil {
		return "", err
	}
	return signedToken, nil
}

func IsMember(username string, password string) bool {
	user, err := database.GetUserByUsername(username)
	// fmt.Printf("IsMember Function, param email:%s password:%s \n", username, password)
	// fmt.Println("IsMember Function, user object: ", user)
	if err != nil {
		return false
	}
	if user.Username == username && user.Password == password {
		return true
	}
	return false

}
func TokenAuthMiddleware() gin.HandlerFunc {
	fmt.Println("AUTHMIDDLEWARE CALLED ....")
	return func(c *gin.Context) {
		tokenInJWTStruct, err := TokenValid(c.Request)
		if err != nil {
			c.JSON(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}
		c.Set("jwt_token", tokenInJWTStruct) //save token in the context
		c.Next()
	}
}

//TokenValid check if the Claims Field available or not. it is considered valid.
func TokenValid(r *http.Request) (*jwt.Token, error) {
	token, err := VerifyToken(r)
	if err != nil {
		return nil, err
	}
	if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
		return nil, err
	}
	return token, nil
}
func VerifyToken(r *http.Request) (*jwt.Token, error) {
	tokenString := ExtractTokenFromHeader(r)
	tokenInJWTStruct, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		//Make sure that the token method conform to "SigningMethodHMAC"
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return JWT_SIGNATURE_KEY, nil
	})

	if err != nil {
		return nil, err
	}
	return tokenInJWTStruct, nil
}
func ExtractTokenFromHeader(r *http.Request) string {
	bearToken := r.Header.Get("Authorization")
	//normally Authorization the_token_xxx
	strArr := strings.Split(bearToken, " ")
	//should return ["Bearer <token>"]
	if len(strArr) == 2 {
		return strArr[1] //return the token only
	}
	return ""
}

func RoleAuthMiddleware(rolesAllowed ...string) gin.HandlerFunc {
	var roles []string
	for _, role := range rolesAllowed {
		roles = append(roles, role)
	}
	return RoleAccessMiddleware(roles)
}

func RoleAccessMiddleware(roleAllowedList []string) gin.HandlerFunc {
	fmt.Println("ROLEACCESSMIDDLEWARE CALLED ....")
	return func(c *gin.Context) {
		tokenString := ExtractTokenFromHeader(c.Request)
		claims := jwt.MapClaims{}
		token, err := jwt.ParseWithClaims(tokenString, &claims, func(token *jwt.Token) (interface{}, error) {
			return JWT_SIGNATURE_KEY, nil
		})
		if err != nil {
			return
		}
		for key, val := range claims {
			fmt.Printf("Key: %v, value: %v\n", key, val)
		}

		if err != nil {
			c.JSON(http.StatusUnauthorized, "Token Not Found: "+err.Error())
			c.Abort()
			return
		}
		// path := c.Request.URL.Path
		// roles := roleAllowedList(RoleAccessList, path)
		tokenRoles := claims["role"].(string)
		// tokenRolesInt, _ := strconv.Atoi(tokenRoles)
		if !Contains(tokenRoles, roleAllowedList) {
			c.JSON(http.StatusUnauthorized, fmt.Sprintf("Limited Access, only allowed for: %v", roleAllowedList))
			c.Abort()
			return
		}
		fmt.Println("roles allowed :", roleAllowedList)

		fmt.Println("jwt :", token)
		fmt.Println("PATH :", c.Request.URL.Path)
		// tokenInJWTStruct, err := TokenValid(c.Request)
		// if err != nil {
		// 	c.JSON(http.StatusUnauthorized, err.Error())
		// 	c.Abort()
		// 	return
		// }
		// c.Set("jwt_token", tokenInJWTStruct) //save token in the context
		c.Next()
	}
}

func roleAllowedList(roleAccessList []RoleAccess, path string) []string {
	var allowedRoles []string
	for _, roleAccess := range roleAccessList {
		if path == roleAccess.Path {
			allowedRoles = append(allowedRoles, roleAccess.AllowedRole)
		}
	}
	return allowedRoles
}

func Contains(element string, slices []string) bool {
	for _, x := range slices {
		if x == element {
			return true
		}
	}
	return false
}
