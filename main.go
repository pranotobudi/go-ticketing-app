package main

import (
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"gitlab.com/pranotobudi/go-ticketing-app/config"
	"gitlab.com/pranotobudi/go-ticketing-app/database"
	"gitlab.com/pranotobudi/go-ticketing-app/database/seeder"
	"gitlab.com/pranotobudi/go-ticketing-app/redis"
	"gitlab.com/pranotobudi/go-ticketing-app/router"
)

// init is invoked before main()
func init() {
	// loads values from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
}

func main() {
	// fmt.Println("bismillah")
	conf := config.New()

	//gorm connection
	dataSourceName := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=disable TimeZone=Asia/Jakarta", conf.Postgresql.Host, conf.Postgresql.User, conf.Postgresql.Password, conf.Postgresql.DBName, conf.Postgresql.Port)

	// //Sql connection
	// connStr := fmt.Sprintf("user=%s port=%d user=%s password=%s sslmode=disable", conf.Postgresql.Host, conf.Postgresql.Port, conf.Postgresql.User, conf.Postgresql.Password)

	database.StartDB(dataSourceName)
	db := database.GetDB()
	database.InitDBTable()
	seeder.DBSeed(10, 10, 10, db)

	r := gin.Default()

	//CRUD user

	r.GET("/users", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin"), redis.CacheMiddlewareByTypeReturned([]database.User{}), router.GetAllUsers)
	r.GET("/users/:id", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "participant"), redis.CacheMiddlewareByTypeReturned(database.User{}), router.GetUser)
	r.GET("/participants", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), redis.CacheMiddlewareByTypeReturned([]database.User{}), router.GetAllParticipants)
	r.DELETE("/users", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin"), router.DeleteAllUsers)
	r.DELETE("/users/:id", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin"), router.DeleteUser)
	r.POST("/users", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin"), router.AddUserJSON)
	r.PUT("/users/:id", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "participant"), router.UpdateUser)

	//CRUD event

	r.GET("/events", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), redis.CacheMiddlewareByTypeReturned([]database.Event{}), router.GetAllEvents)
	r.GET("/events/:id", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), router.GetEvent)
	r.DELETE("/events", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), router.DeleteAllEvents)
	r.DELETE("/events/:id", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), router.DeleteEvent)
	r.POST("/events", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), router.AddEventJSON)
	r.PUT("/events/:id", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), router.UpdateEvent)

	//CRUD transaction

	r.GET("/transactions", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), redis.CacheMiddlewareByTypeReturned([]database.Transaction{}), router.GetAllTransactions)
	r.GET("/transactions/:id", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), router.GetTransaction)
	r.DELETE("/transactions", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), router.DeleteAllTransactions)
	r.DELETE("/transactions/:id", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), router.DeleteTransaction)
	r.POST("/transactions", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), router.AddTransaction)
	r.PUT("/transactions/:id", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), router.UpdateTransaction)

	//Report
	r.GET("/report/event/:id/participant", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), router.GetEventParticipantReport)
	r.GET("/report/event/:id", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), router.GetEventReport)
	r.GET("/report/:creator_id/event/sort/money", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("admin", "creator"), redis.CacheMiddlewareByTypeReturned(database.Report{}), router.GetEventReportSortByMoney)
	r.GET("/report/:creator_id/event/sort/participant", router.RoleAuthMiddleware("admin", "creator"), router.TokenAuthMiddleware(), redis.CacheMiddlewareByTypeReturned(database.Report{}), router.GetEventReportSortByParticipant)

	// Order Ticket
	r.POST("/events/:id/order", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("participant", "admin"), router.OrderTicket)
	r.POST("/events/:id/order/checkout", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("participant", "admin"), router.OrderTicketCheckout)
	r.POST("/events/:id/order/bukti-transfer/upload", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("participant"), router.OrderTicketBuktiTransferUpload)
	r.POST("/events/:id/order/confirmation", router.TokenAuthMiddleware(), router.RoleAuthMiddleware("participant"), router.OrderTicketConfirmation)

	// Registration
	r.POST("/register", router.UserRegisterJSON)
	r.GET("/register/confirmation", router.EmailConfirmation)
	r.POST("/login", router.UserLogin)
	// r.POST("/logout", router.UserLogout)
	// redis.ExampleClient()

	//LEARNING
	// v1 := r.Group("/v1")
	// {
	// 	v1.GET("/v", router.V1)
	// 	v1.GET("/submit", router.V1Submit)
	// }
	if (db.Migrator().HasTable(&database.DBTraining{})) {
		db.Migrator().DropTable(&database.DBTraining{})
	}
	db.Migrator().CreateTable(&database.DBTraining{})
	db.Create(&database.DBTraining{})
	db.Create(&database.DBTraining{})
	//Background Task
	// go tasks.InitEmailSchedulers()

	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")

}
