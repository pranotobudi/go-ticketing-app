package tasks

import (
	"fmt"
	"reflect"
	"time"

	"github.com/go-co-op/gocron"
	"github.com/hibiken/asynq"
	"gitlab.com/pranotobudi/go-ticketing-app/database"
	"gitlab.com/pranotobudi/go-ticketing-app/email"
)

var eventQueue []database.Event

type UnpaidEventsParticipant struct {
	Event        database.Event
	Participants []database.User
}

var unpaidEventsParticipantList []UnpaidEventsParticipant

//
// func myTask() {
// 	fmt.Println("This task will run periodically")
// }

// func ExecuteCronJob() {}
func InitEmailSchedulers() {
	nextEventScheduler := gocron.NewScheduler(time.Now().Location())
	nextEventScheduler.Every(10).Second().Do(SendEmailNextEvent)
	// s.Every(1).Day().At("8:00").Do(myTask)
	nextEventScheduler.StartAsync()

	unpaidEventScheduler := gocron.NewScheduler(time.Now().Location())
	unpaidEventScheduler.Every(15).Second().Do(SendEmailUnpaidEvents)
	// unpaidEventScheduler.Every(1).Hour().Do(SendEmailUnpaidEvents)
	unpaidEventScheduler.StartAsync()

}

func SendEmailNextEvent() {
	event := getFirstEventQueue()
	if event.ID != 0 {
		participants, _ := database.GetAllParticipants()
		for _, participant := range participants {
			EmailNextEventsTask(event, participant)
		}
	} else {
		fmt.Println("next event queue empty")
	}

}

func getFirstEventQueue() database.Event {
	var event database.Event
	if len(eventQueue) != 0 {
		event = eventQueue[0]
		eventQueue = eventQueue[1:]
	}
	return event
}
func AddEventQueue(event database.Event) {
	eventQueue = append(eventQueue, event)
}

func EmailNextEventsTask(event database.Event, participant database.User) {
	msg := []byte("To: " + participant.Email + "\r\n" +
		"Subject: Next Event Information: from Ticketing App !\r\n" +
		"\r\n" +
		" Berikut ini info Next Event: \r\n" +
		"\r\nEvent Title:\r\n" + event.Title_event +
		"\r\nEvent Description:\r\n" + event.Description +
		"\r\n")

	toEmail := []string{participant.Email}
	go email.SendEmail(toEmail, msg)
	fmt.Printf("Email Next Events: event ID: %d  user ID: %d email: %s \n", event.ID, participant.ID, participant.Email)
}

func SendEmailUnpaidEvents() {
	events, _ := database.GetTomorrowEvents()
	for _, event := range events {
		fmt.Printf("eventID: %d\n", event.ID)
		var unpaidParticipants []database.User
		transactions := event.Transactions
		for _, transaction := range transactions {
			if transaction.Status_payment == "failed" {
				user, _ := database.GetUser(transaction.ParticipantID)
				unpaidParticipants = append(unpaidParticipants, user)
			}
		}
		uep := UnpaidEventsParticipant{event, unpaidParticipants}
		if !Find(uep) {
			unpaidEventsParticipantList = append(unpaidEventsParticipantList, uep)
		}
	}
	EmailUnpaidEvent()
}
func Find(uep UnpaidEventsParticipant) bool {
	for _, unpaidEventsParticipant := range unpaidEventsParticipantList {
		if reflect.DeepEqual(unpaidEventsParticipant, uep) {
			return true
		}
	}
	return false
}

func EmailUnpaidEvent() {
	for len(unpaidEventsParticipantList) != 0 {
		unpaidEventParticipant := unpaidEventsParticipantList[0]
		unpaidEventsParticipantList = unpaidEventsParticipantList[1:]

		for _, participant := range unpaidEventParticipant.Participants {
			msg := []byte("To: " + participant.Email + "\r\n" +
				"Subject: Reminder Unpaid Event: from Ticketing App !\r\n" +
				"\r\n" +
				" Jangan lupa melakukan pembayaran: \r\n" +
				"\r\nEvent Title:\r\n" + unpaidEventParticipant.Event.Title_event +
				"\r\nEvent Description:\r\n" + unpaidEventParticipant.Event.Description +
				"\r\n")

			toEmail := []string{participant.Email}
			go email.SendEmail(toEmail, msg)

			fmt.Printf("Email Unpaid Event: event ID: %d userID:%d email: %s \n", unpaidEventParticipant.Event.ID, participant.ID, participant.Email)

		}
	}
}

var asynqClient *asynq.Client

// var asynqServer *asynq.Server

// const redisAddr = "127.0.0.1:6379"

// var rdsAsynq = asynq.RedisClientOpt{Addr: redisAddr}

// func InitAsynqClient() *asynq.Client {
// 	asynqClient = asynq.NewClient(rdsAsynq)
// 	return asynqClient
// }

// func AsynqClientMain() {
// 	// c := InitAsynqClient()
// 	// event := getFirstEventQueue()
// 	// t := EmailNextEventsTask(event)
// 	// _, _ = c.Enqueue(t, asynq.ProcessIn(10*time.Second))

// }

// func InitAsynqServer() *asynq.Server {
// 	asynqServer = asynq.NewServer(rdsAsynq, asynq.Config{
// 		// Specify how many concurrent workers to use
// 		Concurrency: 10,
// 		// Optionally specify multiple queues with different priority.
// 		Queues: map[string]int{
// 			"critical": 6,
// 			"default":  3,
// 			"low":      1,
// 		},
// 		// See the godoc for other configuration options
// 	})

// 	// mux maps a type to a handler
// 	mux := asynq.NewServeMux()
// 	mux.HandleFunc(TypeEmailDelivery, HandleEmailDeliveryTask)
// 	// mux.HandleFunc(TypeEmailNextEvents, HandleEmailNextEventsTask)
// 	if err := asynqServer.Run(mux); err != nil {
// 		log.Fatalf("could not run server: %v", err)
// 	}

// 	return asynqServer
// }
// func AsynqExample() {
// 	//Background Task Client
// 	const redisAddr = "127.0.0.1:6379"
// 	rdsAsynq := asynq.RedisClientOpt{Addr: redisAddr}
// 	c := asynq.NewClient(rdsAsynq)
// 	defer c.Close()

// 	t := NewEmailDeliveryTask(42, "other:template:id")
// 	res, err := c.Enqueue(t, asynq.ProcessIn(10*time.Second))

// 	if err != nil {
// 		log.Fatal("could not schedule task: %v", err)
// 	}
// 	fmt.Printf("Enqueued Result: %+v\n", res)

// 	//Background Task Server

// 	srv := asynq.NewServer(rdsAsynq, asynq.Config{
// 		// Specify how many concurrent workers to use
// 		Concurrency: 10,
// 		// Optionally specify multiple queues with different priority.
// 		Queues: map[string]int{
// 			"critical": 6,
// 			"default":  3,
// 			"low":      1,
// 		},
// 		// See the godoc for other configuration options
// 	})

// 	// mux maps a type to a handler
// 	mux := asynq.NewServeMux()
// 	mux.HandleFunc(TypeEmailDelivery, HandleEmailDeliveryTask)
// 	if err := srv.Run(mux); err != nil {
// 		log.Fatalf("could not run server: %v", err)
// 	}

// }

// // A list of task types.
// const (
// 	TypeEmailDelivery      = "email:deliver"
// 	TypeEmailUnpaidPayment = "email:unpaid_payment"
// 	TypeEmailNextEvents    = "email:next_events"
// )

// //----------------------------------------------
// // Write a function NewXXXTask to create a task.
// // A task consists of a type and a payload.
// //----------------------------------------------

// func NewEmailDeliveryTask(userID int, tmplID string) *asynq.Task {
// 	payload := map[string]interface{}{"user_id": userID, "template_id": tmplID}
// 	return asynq.NewTask(TypeEmailDelivery, payload)
// }

// // func EmailNextEventsTask(event database.Event) *asynq.Task {
// // 	evt, _ := json.Marshal(event)
// // 	payload := map[string]interface{}{
// // 		"event": string(evt),
// // 	}
// // 	return asynq.NewTask(TypeEmailNextEvents, payload)
// // }

// //---------------------------------------------------------------
// // Write a function HandleXXXTask to handle the input task.
// // Note that it satisfies the asynq.HandlerFunc interface.
// //
// // Handler doesn't need to be a function. You can define a type
// // that satisfies asynq.Handler interface. See examples below.
// //---------------------------------------------------------------
// // func HandleEmailNextEventsTask(ctx context.Context, t *asynq.Task) error {
// // 	eventStr, _ := t.Payload.GetString("event")

// // 	var event database.Event
// // 	json.Unmarshal([]byte(eventStr), event)
// // 	// participants, _ := database.GetAllParticipants()
// // 	return nil
// // }

// func HandleEmailDeliveryTask(ctx context.Context, t *asynq.Task) error {

// 	userID, err := t.Payload.GetInt("user_id")
// 	if err != nil {
// 		return err
// 	}
// 	tmplID, err := t.Payload.GetString("template_id")
// 	if err != nil {
// 		return err
// 	}
// 	for i := 0; i < 10; i++ {
// 		fmt.Printf("i=%d\n", i)
// 	}
// 	fmt.Printf("Send Email to User: user_id = %d, template_id = %s\n", userID, tmplID)
// 	// Email delivery code ...
// 	return nil
// }
