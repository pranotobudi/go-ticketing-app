package email

import (
	"fmt"
	"net/smtp"

	"github.com/jordan-wright/email"
	"gitlab.com/pranotobudi/go-ticketing-app/config"
)

func SendEmail(toAddress []string, message []byte) error {
	// Message.
	//   message := []byte("This is a test email message.")
	conf := config.New()
	// Authentication.
	auth := smtp.PlainAuth("", conf.Email.FromAddress, conf.Email.FromEmailPassword, conf.Email.SmtpServer)

	// Sending email.
	err := smtp.SendMail(conf.Email.SmtpServer+":"+fmt.Sprint(conf.Email.SmtpPort), auth, conf.Email.FromAddress, toAddress, message)
	if err != nil {
		fmt.Println(err)
		return err
	}
	fmt.Println("Email Sent Successfully!")
	return nil
}

func SendMailByEmailLib(from string, to string, subject string, htmlMessage string) {

	e := email.NewEmail()
	e.From = "Jordan Wright <test@gmail.com>"
	e.To = []string{"test@example.com"}
	// e.Bcc = []string{"test_bcc@example.com"}
	// e.Cc = []string{"test_cc@example.com"}
	e.Subject = "Awesome Subject"
	// e.Text = []byte("Text Body is, of course, supported!")
	e.HTML = []byte("<h1>Fancy HTML is supported, too!</h1>")

	conf := config.New()
	auth := smtp.PlainAuth("", conf.Email.FromAddress, conf.Email.FromEmailPassword, conf.Email.SmtpServer)

	e.Send(conf.Email.SmtpServer+":"+fmt.Sprint(conf.Email.SmtpPort), auth)

}
